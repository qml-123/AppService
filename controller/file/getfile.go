package file

import (
	"context"
	"errors"

	"github.com/qml-123/AppService/pkg/db"
	"github.com/qml-123/app_log/error_code"
	"github.com/qml-123/app_log/logger"
	"gorm.io/gorm"
)

func GetFile(ctx context.Context, userID int64, file_key string, chunk_num int32) ([]byte, string, int32, bool, error) {
	file_share := &db.FileShare{}
	result := db.GetDB().First(file_share, "file_key = ? and user_id = ? and `delete` = ?", file_key, userID, false)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			logger.Warn(ctx, "db First failed, err: %v", result.Error)
			return nil, "", 0, false, error_code.NoPermission.WithErrMsg(" or file not exist")
		}
		logger.Warn(ctx, "db First failed, err: %v", result.Error)
		return nil, "", 0, false, result.Error
	}
	if !isValidPermission(file_share.Permission) {
		return nil, "", 0, false, error_code.NoPermission
	}
	file := &db.File{}
	result = db.GetDB().First(file, "file_key = ? and chunk_num = ?", file_key, chunk_num)
	if result.Error != nil {
		logger.Warn(ctx, "db First failed, err: %v", result.Error)
		return nil, "", 0, false, result.Error
	}

	return file.Chunk, file.FileType, int32(file.ChunkSize), file.HasMore, nil
}
