// add.h
#ifndef ADD_H
#define ADD_H

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

int32_t add(int32_t a, int32_t b);

#ifdef __cplusplus
}
#endif

#endif // AV1_DECODER_H